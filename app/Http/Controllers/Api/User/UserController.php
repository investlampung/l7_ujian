<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        return UserResource::collection(User::latest()->get());
    }

    public function login(Request $request)
    {
        if (!Auth::attempt(['email'=> $request->email,'password'=>$request->password])) {
            return response()->json([
                'status' => 'failed'
            ]);
        } else {
            return response()->json([
                'status' => 'success'
            ]);
        }
        
    }

    public function store(Request $request)
    {
        $user = User::create($request->all());

        if ($user) {
            return response()->json([
                'status' => 'success'
            ]);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $android_id)
    {
        $user = User::where('android_id', $android_id)->first();
        $user->update($request->all());
        return response(new UserResource($user), Response::HTTP_CREATED);
    }

    public function destroy(User $user)
    {
        $user->delete();
        return response('Deleted', Response::HTTP_OK);
    }
}
