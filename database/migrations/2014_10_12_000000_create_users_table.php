<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * users disebut entitas
     * dan name email dll disebut atribute
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('android_id')->default("text");
            $table->string('kelas')->default("text");
            $table->string('nik')->default("text");
            $table->enum('jenis_kelamin', ['Laki-Laki', 'Perempuan'])->nullable()->default("Laki-Laki");
            $table->enum('status', ['AKTIF', 'TIDAK-AKTIF'])->nullable()->default('AKTIF');
            $table->text('alamat');
            $table->string('no_hp_ortu')->default("text");
            $table->boolean('is_Admin')->default(FALSE);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
